class TweetsController < ApplicationController
    
  def index
    @tweets = Tweet.all
  end
  
  def new
    @tweet = Tweet.new
  end
  
  def create
    tweet = Tweet.new(message: params[:tweet][:message],tdate: params[:tweet][:tdate])
    if tweet.save
     redirect_to root_path
    else
     flash[:notice] = "エラー"
     redirect_to new_tweet_path
    end
  end
  
  def destroy
    tweet = Tweet.find(params[:id])
    tweet.destroy
    redirect_to root_path
  end
  
  def show
    @tweet = Tweet.find(params[:id])
  end
  
  def edit
    @tweet = Tweet.find(params[:id])
  end

  def update
    message= params[:tweet][:message]
    tdate = params[:tweet][:message]
    @tweet = Tweet.find(params[:id])
    @tweet.update(message: message, tdate: tdate)
    if @tweet.save
      flash[:notice] = "1 record update"
      redirect_to root_path
    else
      render 'edit'
    end
  end
  
end
